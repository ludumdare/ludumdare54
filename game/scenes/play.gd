extends Node2D

@onready var _player = $Entities/PlayerEntity
@export var icon_saw: PackedScene
@export var icon_slime: PackedScene
@export var icon_floor: PackedScene
@export var icon_acid: PackedScene


enum {
	
	PLAY_NONE,
	PLAY_INIT,
	PLAY_START,
	PLAY_RUN,
	PLAY_NEXT,
	PLAY_IDLE,
	PLAY_STOP,
	PLAY_QUIT,
	PLAY_RELOAD,
	PLAY_DEATH,
	PLAY_PICK,
	PLAY_RETRY,
	PLAY_HOST,
	PLAY_LOSE,
	PLAY_WIN,
}

enum {
	ROOM_SLIME,
	ROOM_WALL,
	ROOM_SAW,
	ROOM_ACID,
	ROOM_LASER,
	ROOM_BEAR,
	ROOM_FLOOR,
	ROOM_RANDOM
}



var _state: int = PLAY_NONE
var _next_state: int = PLAY_NONE
var _dead: bool = false
var _welcome_line = 0
var _door = "slime"
var d1name
var d2name
var d3name
var d4name

func _ready():
	
	Global.player_death.connect(_on_player_death)
	Global.host_next.connect(_on_host_next)
	Global.host_skip.connect(_on_host_skip)
	_change_state(PLAY_INIT)
	
	Global.level = 0
	

func _process(delta):
	
	match (_state):
		
		PLAY_NONE:
			Logger.trace("PLAY_NONE")
	
		PLAY_INIT:
			Logger.trace("PLAY_INIT")
			$DoorsClosed.visible = true
			$DoorsOpen.visible = false
			$DoorIcons.visible = false
			$UI/HOST.visible = false
			$UI/WIN.visible = false
			$UI/LOSE.visible = false
			$UI/PLAY.visible = false
			Global.occupied = {}
			_create_level()
			print("level %s" % Global.level)
			_player.position = Vector2(400,240)
			Game.play_music("Play")
			ECS.update("init")
	
		PLAY_START:
			Logger.trace("PLAY_START")
			$RoomTImer.start()
			$MoneyTimer.start()
			$UI/PLAY.visible = true
			ECS.update("start")
			_change_state(PLAY_RUN)
	
		PLAY_RUN:
			Logger.trace("PLAY_RUN")
			ECS.update("run")
			
		PLAY_STOP:
			Logger.trace("PLAY_STOP")
			Game.play_music("Next")
			_change_state(PLAY_QUIT)
			ECS.clean()
			ECS.update("run")
			
		PLAY_QUIT:
			Logger.trace("PLAY_QUIT")
			_change_state(PLAY_NONE)
			Game.set_state(Game.GameStates.HOME)
			ECS.update("idle")	
			
		PLAY_NEXT:
			Logger.trace("PLAY_NEXT")
			Game.play_music("Next")
			_clean_level()
			_create_doors()		
			Global.level += 1
			$DoorsClosed.visible = false
			$DoorsOpen.visible = true
			$DoorIcons.visible = true
			ECS.update("idle")
			if Global.level < 100:
				_change_state(PLAY_PICK)
			else:
				_change_state(PLAY_WIN)
			
		PLAY_DEATH:
			Logger.trace("PLAY_DEATH")
			ECS.update("death")
			
		PLAY_PICK:
			Logger.trace("PLAY_PICK")
			ECS.update("pick")
			
		PLAY_RETRY:
			Game.play_music("Next")
			_clean_level()		
			Global.level = 0
			_dead = false
			ECS.update("idle")
			_change_state(PLAY_INIT)
			
		PLAY_HOST:
			Logger.trace("PLAY_HOST")
			ECS.update("idle")
			
		PLAY_IDLE:
			Logger.trace("PLAY_IDLE")
			
		PLAY_LOSE:
			Logger.trace("PLAY_LOSE")
			$UI/LOSE.visible = true
			_change_state(PLAY_IDLE)
			
		PLAY_WIN:
			Logger.trace("PLAY_WIN")
			$UI/WIN.visible = true
			_change_state(PLAY_IDLE)
			
	if Input.is_action_just_pressed("ui_cancel"):
		_change_state(PLAY_STOP)
		
	if Input.is_action_just_pressed("game_next"):
		_change_state(PLAY_NEXT)
			
	_state = _next_state
	
	var _debug = ""
	_debug += "MONEY: %s \n" % Global.money
	_debug += "LEVEL: %s \n" % Global.level
	_debug += "TIMER: %1.1f \n" % $RoomTImer.time_left
	
	$UI/PLAY/Debug.text = _debug
	
	
func _change_state(state):
	_next_state = state


func _on_player_death(name, trap):
	
	if _dead:
		return
		
	_dead = true
	
	$DeathTimer.start()
	$RoomTImer.stop()
	$MoneyTimer.stop()
	
	Game.play_music("Lose")
	Game.play_sound("Laugh")
	
	_change_state(PLAY_DEATH)
	
	var _death = Global.player_death_scene.instantiate()
	trap.add_child(_death)
	_death.global_position = $Entities/PlayerEntity.global_position

	$Entities/PlayerEntity.position = Vector2(-1000,-1000)

	if name == "saw":	
		_death.play()
		
	if name == "slime":
		_death.global_position = trap.global_position
		Game.play_sound("Slime")

	
func _clean_level():
	for n in $Entities/Root.get_children():
		$Entities/Root.remove_child(n)
		n.queue_free()
			


func _on_money_timer_timeout():
	_add_money()
	
		
func _add_money():
	var _rloc = Global.get_random_location()
	var _money = Global.money_scene.instantiate()
	$Entities/Root.add_child(_money)
	_money.name = "Money"
	_money.position = Global.get_grid_position(_rloc)


func _on_room_timer_timeout():
	$MoneyTimer.stop()
	Game.play_sound("Clap")
	_change_state(PLAY_NEXT)


func _on_death_timer_timeout():
	_change_state(PLAY_LOSE)


func _on_door_1_body_entered(body):
	if _state == PLAY_PICK:
		_change_state(PLAY_INIT)
		_door = d1name


func _on_door_2_body_entered(body):
	if _state == PLAY_PICK:
		_change_state(PLAY_INIT)
		_door = d2name


func _on_door_3_body_entered(body):
	if _state == PLAY_PICK:
		_change_state(PLAY_INIT)
		_door = d3name


func _on_door_4_body_entered(body):
	if _state == PLAY_PICK:
		_change_state(PLAY_INIT)
		_door = d4name


func _create_doors():
	
	var _types = ["slime", "saw", "acid", "floor"]
	
	var _d1 = randi_range(0,_types.size()-1)
	d1name = _types[_d1]
	_types.remove_at(_d1)
	
	var _d2 = randi_range(0,_types.size()-1)
	d2name = _types[_d2]
	_types.remove_at(_d2)
	
	var _d3 = randi_range(0,_types.size()-1)
	d3name = _types[_d3]
	_types.remove_at(_d3)
	
	var _d4 = randi_range(0,_types.size()-1)
	d4name = _types[_d4]
	_types.remove_at(_d4)
	
	print(d1name)
	print(d2name)
	print(d3name)
	print(d4name)
	
	for child in $DoorIcons/Icon1/Icon.get_children():
		child.queue_free()
	
	for child in $DoorIcons/Icon2/Icon.get_children():
		child.queue_free()
	
	for child in $DoorIcons/Icon3/Icon.get_children():
		child.queue_free()
	
	for child in $DoorIcons/Icon4/Icon.get_children():
		child.queue_free()
		
	_create_door_icon($DoorIcons/Icon1/Icon, d1name)
	_create_door_icon($DoorIcons/Icon2/Icon, d2name)
	_create_door_icon($DoorIcons/Icon3/Icon, d3name)
	_create_door_icon($DoorIcons/Icon4/Icon, d4name)
	
		
func _create_door_icon(door, icon):
	
	var _icon 
	
	if icon == "slime":
		_icon = icon_slime.instantiate()
		door.add_child(_icon)
		return _icon
	
	if icon == "acid":
		_icon = icon_acid.instantiate()
		door.add_child(_icon)
		return _icon
	
	if icon == "floor":
		_icon = icon_floor.instantiate()
		door.add_child(_icon)
		return _icon
	
	if icon == "saw":
		_icon = icon_saw.instantiate()
		door.add_child(_icon)
		return _icon
	
	

func _create_level():
	
	if Global.level == 0:
		_host_message("welcome")		
		$UI/HOST.visible = true
		_change_state(PLAY_HOST)
		return
	
	var _traps = []
	var _picked = []
	var _trapgenerator = $Entities/TrapGeneratorEntity.get_component("trapgeneratorcomponent") as TrapGeneratorComponent
#	_trapgenerator.traps = [Global.TRAP_SLIME, Global.TRAP_ACID, Global.TRAP_SAW, Global.TRAP_FLOOR]
#	_trapgenerator.traps = [Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SAW]
#	_trapgenerator.traps = [Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SAW, Global.TRAP_SAW, Global.TRAP_SAW]

	for i in range(25):
		_traps.append(Global.TRAP_SLIME)
		
	for i in range(25):
		_traps.append(Global.TRAP_ACID)
		
	for i in range(25):
		_traps.append(Global.TRAP_FLOOR)
		
	for i in range(10):
		_traps.append(Global.TRAP_SAW)

	for i in range((Global.level / 10) + 5):
		var _i = randi_range(0, _traps.size()-1)
		_picked.append(_traps[_i])
		
	for i in range((Global.level / 25) + 2):
		
		if _door == "slime":
			_picked.append(Global.TRAP_SLIME)
		
		if _door == "saw":
			_picked.append(Global.TRAP_SAW)
		
		if _door == "acid":
			_picked.append(Global.TRAP_ACID)
		
		if _door == "FLOOR":
			_picked.append(Global.TRAP_FLOOR)
		
		
	_trapgenerator.traps = _picked
		
		
	_change_state(PLAY_START)
	

func _on_home_pressed():
	_change_state(PLAY_STOP)
	
	
func _host_message(id):
	_welcome_line = 0
	Global.host_message.emit(Global.welcome_lines[_welcome_line])
	_change_state(PLAY_HOST)
	
	
func _on_play_pressed():
	_change_state(PLAY_RETRY)
	
	
func _on_host_next():
	print("next")
	_welcome_line += 1
	
	if _welcome_line >= Global.welcome_lines.size():
		_on_host_skip()
		return
		
	Global.host_message.emit(Global.welcome_lines[_welcome_line])
	
	
func _on_host_skip():
	print("skip")
	$UI/HOST.visible = false
	_change_state(PLAY_NEXT)
