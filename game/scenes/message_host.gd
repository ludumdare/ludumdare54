extends Control

func _ready():
	
	Global.host_message.connect(_on_host_message)
	
func _on_host_message(message):
	$MessagePanel/Message.text = message


func _on_skip_pressed():
	Global.host_skip.emit()


func _on_next_pressed():
	Global.host_next.emit()
