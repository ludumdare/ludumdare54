extends Node2D

var ticks = 0


func _process(delta):
	
	ticks += 1
	
	if ticks > 10:
		ECS.clean()
		SceneManager.transition_to("res://game/scenes/play.tscn")
