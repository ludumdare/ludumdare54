extends Node2D

@export var player_death_scene: PackedScene
@export var slime_death_scene: PackedScene
@export var money_scene: PackedScene

var money = 0
var occupied = {}
var level = 0
var traps = []
var welcome_lines = []

const GRID_SIZE = 64
const GRID_OFFSET = Vector2i.ONE * (GRID_SIZE / 2)

signal money_collected
signal player_death(name, trap)
signal host_next
signal host_skip
signal host_message(message)

const TRAP_NONE = "none"
const TRAP_SAW = "saw"
const TRAP_SLIME = "slime"
const TRAP_ACID = "acid"
const TRAP_FLOOR = "floor"

func _ready():
	
	Game.load_sounds($Sounds)
	Game.load_music($Music)

	Game.on_state_changed.connect(_on_state_changed)
	money_collected.connect(_on_money_collected)
	
	# load host welcome lines
	welcome_lines = []
	var config = ConfigFile.new()
	config.load("res://host.ini")
	var lines = config.get_section_keys("welcome")
	for line in lines:
		var _line = config.get_value("welcome", line)
		welcome_lines.append(_line)
	
	print(welcome_lines)
	
	

func _on_money_collected():
	money += 1
	Logger.info("player has %s money" % money)
	Game.play_sound("Money")


func _on_state_changed(state):
	
	if state == Game.GameStates.HOME:
		
		Game.play_music("Title")
	
	if state == Game.GameStates.PLAY:
		
		occupied = {}
		money = 0
		level = 0
		traps = []
	
"""
test test test
"""
func get_grid_position(location: Vector2i):
	return (location * GRID_SIZE) - GRID_OFFSET


func moveto_grid_position(entity: Node2D, location: Vector2i):
	entity.global_position = get_grid_position(location)


func get_random_location():
	
	var _x = randi_range(0, 9) + 2
	var _y = randi_range(0, 5) + 2
	
	return Vector2i(_x, _y)
