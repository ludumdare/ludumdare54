extends System
class_name SlimeCollisionSystem


func on_process_entity(entity : Entity, delta : float):
	
	Logger.trace("[SlimeCollisionSystem] on_process_entity")
	
	var _move = entity.get_component("movecomponent") as MoveComponent
	var _collision = entity.get_component("collisioncomponent") as CollisionComponent
	
	if (_collision.has_collision):
		
		var _name = _collision.collision.get_collider().name.to_lower() 
		
		Logger.trace("slime collided with %s" % _name)
		
		if _name.contains("player"):
			return
			
		var _death = Global.slime_death_scene.instantiate()
		add_child(_death)
		_death.position = entity.position
		_death.play()
		ECS.remove_entity(entity)

