extends System
class_name PlayerCollisionSystem

func on_process_entity(entity : Entity, delta : float):
	
	Logger.trace("[PlayerControllerSystem] on_process_entity")
	
	var _move = entity.get_component("movecomponent") as MoveComponent

	var _collision = entity.get_component("collisioncomponent") as CollisionComponent
	
	if (_collision.has_collision):
		
		var _name = _collision.collision.get_collider().name.to_lower() 
		
		Logger.trace("player collided with %s" % _name)
				
		if _name.contains("money"):
			
			Logger.info("collected money")
			Global.money_collected.emit()
			_collision.collision.get_collider().position = Vector2(-1000,-1000)

		if _name.contains("slime"):
			
			Logger.fine("ran into slime")
			Global.player_death.emit("slime", _collision.collision.get_collider())
			

		if _name.contains("floor"):
			
			Logger.fine("ran into floor")
			Global.player_death.emit("floor", _collision.collision.get_collider())
			

		if _name.contains("acid"):
			
			Logger.fine("ran into acid")
			Global.player_death.emit("acid", _collision.collision.get_collider())
			

		if _name.contains("saw"):
			
			Logger.fine("ran into saw")
			Global.player_death.emit("saw", _collision.collision.get_collider())
