extends System
class_name MovetoSystem

func on_process_entity(entity : Entity, delta : float):
	
	Logger.trace("[MovetoSystem] on_process_entity")
	
	var _move = entity.get_component("movecomponent") as MoveComponent
	var _moveto = entity.get_component("movetocomponent") as MovetoComponent
	
	if _moveto.is_moving:
		
		# calculate next position
		
		var _from_position = Global.get_grid_position(_moveto.from_location) as Vector2
		var _next_position = Global.get_grid_position(_moveto.next_location) as Vector2
		
		Logger.trace("from position %s" % _from_position)
		Logger.trace("next position %s" % _next_position)
		Logger.trace("entity position %s" % entity.position.floor())
		
		var _distance = entity.position.distance_to(_next_position)
		
		Logger.trace("distance %s" % _distance)
		
		if abs(entity.position.distance_to(_next_position)) < 2:
			entity.position = Global.get_grid_position(_moveto.next_location)
			_move.direction = Vector2.ZERO
			_moveto.is_moving = false
			return
		
		# calculate direction
		
		_move.direction = entity.position.direction_to(_next_position)
		
		Logger.trace("next direction %s" % _move.direction)

	else:
		
		# figure out next grid position to move to
		
		# get number from -1 to 1 for both x & y
		
		var _x = randi_range(-1, 1)
		var _y = randi_range(-1, 1)
		
		_moveto.from_location = _moveto.next_location
		_moveto.next_location = _moveto.from_location + Vector2i(_x, _y)
		
		_moveto.next_location.x = clampi(_moveto.next_location.x, 2, 11)
		_moveto.next_location.y = clampi(_moveto.next_location.y, 2, 7)
				
		Logger.trace("next location %s" % _moveto.next_location)
		
		_moveto.is_moving = true
