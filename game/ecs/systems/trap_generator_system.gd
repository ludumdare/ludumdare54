extends System
class_name TrapGeneratorSystem

@export var root_node: Node
@export var slime_trap: PackedScene
@export var saw_trap: PackedScene
@export var acid_trap: PackedScene
@export var saw_floor: PackedScene
@export var trap_floor: PackedScene

func on_process_entity(entity : Entity, delta : float):
	
	var _trapgenerator = entity.get_component("trapgeneratorcomponent") as TrapGeneratorComponent
	
	for trap in _trapgenerator.traps:
		
		print("next trap is %s" % trap)
		
		var _done = false

		while ! _done:
			
			# check for open spot		
			var _rloc = Global.get_random_location()
			
			Logger.trace("while")
			
			# nothing allowed on 4th row (where player spawns)
			if _rloc.y == 4:
				continue
				
			if trap == Global.TRAP_ACID:
				
				Logger.fine("%s" % Global.occupied)
				Logger.fine("trying to fill %s" % _rloc)
						
				if Global.occupied.has(_rloc):
					continue

				Global.occupied[_rloc] = Global.TRAP_ACID
				_done = true
			
				var _rpos = Global.get_grid_position(_rloc)
				
				var _acid = acid_trap.instantiate()
				root_node.add_child(_acid)
				_acid.position = _rpos	
				_acid.name = "Acid"
				

			if trap == Global.TRAP_FLOOR:
				
				Logger.fine("%s" % Global.occupied)
				Logger.fine("trying to fill %s" % _rloc)
						
				if Global.occupied.has(_rloc):
					continue

				Global.occupied[_rloc] = Global.TRAP_FLOOR
				_done = true
			
				var _rpos = Global.get_grid_position(_rloc)
				
				var _floor = trap_floor.instantiate()
				root_node.add_child(_floor)
				_floor.position = _rpos	
				_floor.name = "Floor"
				

			if trap == Global.TRAP_SLIME:
				
				Logger.fine("%s" % Global.occupied)
				Logger.fine("trying to fill %s" % _rloc)
						
				if Global.occupied.has(_rloc):
					continue

				Global.occupied[_rloc] = Global.TRAP_SLIME
				_done = true
		
				var _rpos = Global.get_grid_position(_rloc)
				
				var _slime = slime_trap.instantiate()
				root_node.add_child(_slime)
				_slime.position = _rpos	
				_slime.name = "Slime"
				
				var _moveto = _slime.get_component("movetocomponent") as MovetoComponent
				_moveto.next_location = _rloc
				
	
			if trap == Global.TRAP_SAW:
				
				_rloc.x = clampi(_rloc.x, 2, 8)
				_rloc.y = clampi(_rloc.y, 2, 7)
				
				Logger.fine("%s" % Global.occupied)
				Logger.fine("trying to fill %s" % _rloc)
						
				if Global.occupied.has(_rloc):
					continue

				if Global.occupied.has(_rloc + Vector2i.RIGHT * 1):
					continue

				if Global.occupied.has(_rloc + Vector2i.RIGHT * 2):
					continue

				if Global.occupied.has(_rloc + Vector2i.RIGHT * 3):
					continue

				Global.occupied[_rloc] = Global.TRAP_SAW
				Global.occupied[_rloc + Vector2i.RIGHT * 1] = Global.TRAP_SAW
				Global.occupied[_rloc + Vector2i.RIGHT * 2] = Global.TRAP_SAW
				Global.occupied[_rloc + Vector2i.RIGHT * 3] = Global.TRAP_SAW
				_done = true
				
				var _rpos = Global.get_grid_position(_rloc)
				
				var _saw = saw_trap.instantiate()
				root_node.add_child(_saw)
				_saw.position = _rpos
				_saw.name = "Saw"
				
				var _sawfloor = saw_floor.instantiate()
				root_node.add_child(_sawfloor)
				_sawfloor.position = _rpos
				_sawfloor.name = "SawFloor"
				
				var _sawtrap = _saw.get_component("sawtrapcomponent") as SawTrapComponent

				# calculate saw end position as 3 grid spaces to the right
				_sawtrap.start_location = _rloc
				_sawtrap.end_location = _sawtrap.start_location + Vector2i.RIGHT * 3
				
				# create tween
				var _start_position = _saw.position
				Logger.fine("start position is %s" % _saw.position)
				var _end_position: Vector2 = Global.get_grid_position(_sawtrap.end_location)
				Logger.fine("end position is %s" % _end_position)
				
				
#				var _tween = create_tween()
				var _tween = create_tween().bind_node(_saw)
				_tween.set_trans(Tween.TRANS_CUBIC)
				_tween.set_ease(Tween.EASE_IN_OUT)
				_tween.set_loops()
				_tween.tween_property(_saw, "position", _end_position, _sawtrap.speed)
				_tween.tween_property(_saw, "position", _start_position, _sawtrap.speed)

