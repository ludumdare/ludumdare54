class_name MoveSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	Logger.trace("[MoveSystem] on_process_entity")
	
	var _move = entity.get_component("movecomponent") as MoveComponent
	var _collision = entity.get_component("collisioncomponent") as CollisionComponent
	
	entity.velocity = _move.direction * _move.SPEED * _move.SPEED
	
	Logger.fine("velocity is %s" % entity.velocity)
	
	var _has_collided = entity.move_and_slide()
	
	Logger.fine("entity %s collided = %s" % [entity.name, _has_collided])
	
	_collision.has_collision = false
	
	if (_has_collided):
		
		_collision.has_collision = true
		_collision.collision = entity.get_slide_collision(0)
