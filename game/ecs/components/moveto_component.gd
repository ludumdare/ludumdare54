extends Component
class_name MovetoComponent

var is_moving: bool = false
var from_location: Vector2i = Vector2i.ZERO
var next_location: Vector2i = Vector2i.ZERO
