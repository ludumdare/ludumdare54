class_name MoveComponent
extends Component

@export var SPEED : float = 50
@export var SPEED_FACTOR : float = 1

@export var direction := Vector2.ZERO
