extends Component
class_name SawTrapComponent

var start_location: Vector2i = Vector2i.ZERO
var end_location: Vector2i = Vector2i.ZERO
var speed: float = 1.0
