extends Node2D

func _process(delta):
	ECS.update()
	


func _ready():
	
	var _slime = $Room/SlimeTrapEntity as SlimeTrapEntity
	
	var _moveto = _slime.get_component("movetocomponent") as MovetoComponent
	
	_moveto.next_location = Vector2i(5,5)
	Global.moveto_grid_position(_slime, _moveto.next_location)
