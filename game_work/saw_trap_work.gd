extends Node2D

func _ready():
	
	var _sawtrap = $SawTrapEntity.get_component("sawtrapcomponent") as SawTrapComponent
	
	_sawtrap.start_location = Vector2i(5, 5)
	$SawTrapEntity.position = Global.get_grid_position(_sawtrap.start_location)
	
	# calculate saw end position as 3 grid spaces to the right
	Logger.debug("start location is %s" % _sawtrap.start_location)
	_sawtrap.end_location = _sawtrap.start_location + Vector2i.RIGHT * 3
	Logger.debug("end location is %s" % _sawtrap.end_location)
	
	# create tween
	
	var _start_position = $SawTrapEntity.position
	Logger.debug("start position is %s" % $SawTrapEntity.position)
	var _end_position: Vector2 = Global.get_grid_position(_sawtrap.end_location)
	Logger.debug("end position is %s" % _end_position)
	
	
	var _tween = get_tree().create_tween()
	_tween.set_trans(Tween.TRANS_CUBIC)
	_tween.set_ease(Tween.EASE_IN_OUT)
	_tween.set_loops()
	_tween.tween_property($SawTrapEntity, "position", _end_position, _sawtrap.speed)
	_tween.tween_property($SawTrapEntity, "position", _start_position, _sawtrap.speed)
	
	Logger.debug("tween running %s" % _tween.is_running())
	
	
