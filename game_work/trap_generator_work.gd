extends Node2D

enum {
	
	PLAY_NONE,
	PLAY_INIT,
	PLAY_START,
	PLAY_RUN,
	PLAY_IDLE,
	PLAY_STOP,
	PLAY_QUIT
}

var _state: int = PLAY_NONE
var _next_state: int = PLAY_NONE

func _ready():
	
	Global.occupied = {}
	
	var _trapgenerator = $TrapGeneratorEntity.get_component("trapgeneratorcomponent") as TrapGeneratorComponent
	_trapgenerator.traps = [Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SLIME, Global.TRAP_SAW, Global.TRAP_SAW, Global.TRAP_SAW]
	
	_change_state(PLAY_INIT)

func _process(delta):
	
	match (_state):
		
		PLAY_NONE:
			Logger.trace("PLAY_NONE")
	
		PLAY_INIT:
			Logger.trace("PLAY_INIT")
			_change_state(PLAY_START)
			ECS.update("init")
	
		PLAY_START:
			Logger.trace("PLAY_START")
			_change_state(PLAY_RUN)
			ECS.update("start")
	
		PLAY_RUN:
			Logger.trace("PLAY_RUN")
			ECS.update("run")
			
		PLAY_STOP:
			Logger.trace("PLAY_STOP")
			_change_state(PLAY_QUIT)
			ECS.clean()
			ECS.update("run")
			
		PLAY_QUIT:
			Logger.trace("PLAY_QUIT")
			_change_state(PLAY_NONE)
			ECS.update("run")
			SceneManager.transition_to("res://game_work/trap_generator_work.tscn")
			
		PLAY_IDLE:
			Logger.trace("PLAY_IDLE")
			
	if Input.is_key_pressed(KEY_ESCAPE):
		_change_state(PLAY_STOP)
			
	_state = _next_state
	
	
func _change_state(state):
	
	_next_state = state
